from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Post, PostCategory, PostSubcategory
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import logout, authenticate, login
from django.contrib import messages
from .forms import NewUserForm

# Create your views here.
# For returning posts and categories based on slug
def single_slug(request, single_slug):
    # Create list of existing categories
    categories = [c.category_slug for c in PostCategory.objects.all()]

    # Return category with related-posts based on slug
    if single_slug in categories:
        # Queryset
        matching_posts = Post.objects.filter(post_category__category_slug=single_slug)
        matching_posts_cat = matching_posts[0]
        post_urls = {}

        return render(
            request = request,
            template_name = 'main/category.html',
            context = {'posts_in_category': matching_posts, 'matching_posts_cat': matching_posts_cat}
        )
    
    # Return post data based on slug
    posts = [p.post_slug for p in Post.objects.all()]
    if single_slug in posts:
        our_post = Post.objects.get(post_slug=single_slug)
        
        return render(
            request = request,
            template_name = 'main/post_detail.html',
            context = {
                'post': our_post
            }
        )

    if single_slug == 'account':
        return render(
            request = request,
            template_name = 'main/account.html'
        )

    # Naive error handling
    return HttpResponse(f"'{single_slug}' does not exist.")

def homepage(request):
    return render(
        request = request,
        template_name = 'main/home.html',
        context = {'posts': Post.objects.all}
    )
    #return HttpResponse('Homepage test')
    
def register(request):
    if request.method == 'POST':
        form = NewUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'New account created: {username}')
            login(request, user)
            return redirect('main:homepage')

        # Error handling
        else:
            for msg in form.error_messages:
                messages.error(request, f'{msg}: {form.error_messages[msg]}')
            
            return render(
                request = request,
                template_name = 'main/register.html',
                context = {'form': form}
            )

    form = NewUserForm
    return render(
        request = request,
        template_name = 'main/register.html',
        context = {'form': form}
        )

def logout_request(request):
    logout(request)
    messages.info(request, 'Logged out successfully')
    return redirect('main:homepage')

def login_request(request):
    if request.method == 'POST':
        form = AuthenticationForm(request=request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                messages.info(request, f'You are now logged in as {username}.')
                return redirect('/')
            else:
                messages.error(request, 'Invalid username or password.')
        else:
            messages.error(request, 'Invalid username or password.')
    form = AuthenticationForm()
    return render(
        request = request,
        template_name = 'main/login.html',
        context = {'form': form}
    )
