# django-general-webapp
### General use web application using Django

Barebones content management suited to blogs, news websites, product landing pages and the like. 
Uses TinyMCE admin-side to allow easy access to text formatting and image/video sharing.

All you need to get going for someone completely new to web development. 
Some ideas for what you can do next: proper error handling, search, account page, slicker CSS, toasts, complex URLs etc. 
