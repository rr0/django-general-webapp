from django.contrib import admin
from .models import Post, PostCategory, PostSubcategory
from tinymce.widgets import TinyMCE
from django.db import models

# Register your models here.
# Change display settings for our model attributes on admin
class PostAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Title/date', {'fields': ['post_title', 'post_published', 'post_updated']}),
        ('Meta', {'fields': ['post_category', 'post_subcategory', 'post_slug']}),
        ('Content', {'fields': ['post_content']}),
    ]

    formfield_overrides = {
        models.TextField: {'widget': TinyMCE()},
    }

admin.site.register(Post, PostAdmin)
admin.site.register(PostCategory)
admin.site.register(PostSubcategory)
