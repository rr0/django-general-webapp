from django.db import models
from datetime import datetime

# Create your models here.
class PostCategory(models.Model):
    post_category = models.CharField(max_length=200)
    category_summary = models.CharField(max_length=200)
    category_slug = models.CharField(max_length=200, default=1)

    class Meta:
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.post_category

class PostSubcategory(models.Model):
    post_subcategory = models.CharField(max_length=200)
    post_category = models.ForeignKey(PostCategory, default=1, verbose_name='Category', on_delete=models.SET_DEFAULT)
    subcategory_summary = models.CharField(max_length=200)
    post_subcategory_slug = models.CharField(max_length=200, default=1)

    class Meta:
        verbose_name_plural = 'Subcategories'
    
    def __str__(self):
        return self.post_subcategory

class Post(models.Model):
    post_title = models.CharField(max_length=200)
    post_content = models.TextField()
    author = models.CharField(max_length=200, default='Admin')
    post_published = models.DateTimeField('date published', default=datetime.now)
    post_updated = models.DateTimeField('date updated', default=datetime.now)
    #https://docs.djangoproject.com/en/2.1/ref/models/fields/#django.db.models.ForeignKey.on_delete
    post_category = models.ForeignKey(PostCategory, default=1, verbose_name='Category', on_delete=models.SET_DEFAULT)
    post_subcategory = models.ForeignKey(PostSubcategory, default=1, verbose_name='Subcategory', on_delete=models.SET_DEFAULT)
    post_slug = models.CharField(max_length=200, default=1)

    def __str__(self):
        return self.post_title